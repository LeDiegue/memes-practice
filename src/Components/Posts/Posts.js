import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default function Posts() {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((json) => setPosts(json));
  }, []);
  
  return (
    <div>
      {posts.map((item) => (
        <div key={item.id}>
          <Link to={"/posts/" + item.id} className="text-decoration-none">
            <h4>{item.title}</h4>
          </Link>
          <p>{item.body}</p>
        </div>
      ))}
    </div>
  );
}
