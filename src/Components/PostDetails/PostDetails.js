import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Comments from "../Comments/Comments";

export default function PostDetails() {
  const [post, setPost] = useState([]);
  let { id } = useParams();

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts/" + id)
      .then((response) => response.json())
      .then((json) => setPost(json));
  }, []);
  
  return (
    <div className="container">
      {post ? (
        <>
          <h3 className="text-primary mt-4">{post.title}</h3>
          <p className="text-muted mb-3 border-bottom">
            By user n°{post.userId}
          </p>
          <p className="mb-2">{post.body}</p>
          <Comments />
        </>
      ) : (
        <h4>No se encontró el post :c</h4>
      )}
    </div>
  );
}
