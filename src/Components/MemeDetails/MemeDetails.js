import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

export default function MemeDetails() {
    const { id } = useParams();    // { id: 10 }
    const [meme, setMeme] = useState('');

    useEffect(() => {
        let memes = JSON.parse(localStorage.getItem('memesList')) || [];

        setMeme(memes[id]);
    }, [])

    return (
        <div className="container">
            <div className="row mt-4">
                <h4>{meme.title}</h4>
                <img src={meme.url} className="w-100"/>
            </div>
        </div>
    )
}