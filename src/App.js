import React, { useState } from "react";
import Navbar from "./Components/Navbar/Navbar";
import Posts from "./Components/Posts/Posts";
import MemeDetails from "./Components/MemeDetails/MemeDetails";
import PostDetails from "./Components/PostDetails/PostDetails";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./App.css";

function App() {
  const [memes, setMemes] = useState(
    JSON.parse(localStorage.getItem("memesList")) || []
  );
  const [nuevoMeme, setNuevoMeme] = useState({
    title: "",
    url: "",
  });

  function saveMeme() {
    console.log(nuevoMeme);
    setMemes([...memes, nuevoMeme]);
    setTimeout(() => {
      localStorage.setItem("memesList", JSON.stringify(memes));
    }, 500);
  }

  function handleChange(e) {
    setNuevoMeme({ ...nuevoMeme, [e.target.name]: e.target.value });
  }

  return (
    <Router>
      <Navbar />
      <Switch>
        <Route path='/posts/:id'>
          <PostDetails />
        </Route>
        <Route path="/posts">
          <Posts />
        </Route>
        <Route path="/:id">
          <MemeDetails />
        </Route>
        <Route exact path="/">
          <div className="container">
            <div className="row mt-4">
              <div className="col-12">
                <button type="button" className="btn btn-success" data-toggle="modal" data-target="#exampleModal" >
                  Agregar Meme
                </button>

                <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                  <div className="modal-dialog" role="document">
                    <div className="modal-content">
                      <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">
                          Agregar Meme
                        </h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" >
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div className="modal-body">
                        <input type="text" name="title" placeholder="titulo" value={nuevoMeme.title} onChange={(e) => handleChange(e)} />
                        <input type="text" name="url" placeholder="url meme" value={nuevoMeme.url} onChange={(e) => handleChange(e)} />
                      </div>
                      <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" >
                          Close
                        </button>
                        <button type="button" className="btn btn-primary" onClick={() => saveMeme()} data-dismiss="modal" >
                          Save changes
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-4">
              {memes.map((item, i) => (
                <div className="col-3 mb-4" key={i}>
                  <Link to={`/${i}`}>
                    <img src={item.url} className="w-100" alt={item.title} />
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;