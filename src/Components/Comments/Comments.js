import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

export default function Comments() {
  const [comments, setComments] = useState([]);
  let { id } = useParams();

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts/" + id + "/comments")
      .then((response) => response.json())
      .then((json) => setComments(json));
  }, []);

  return (
    <div className="row mt-5">
        <h4 className="mb-2">Comments</h4>
        {
            comments.map((c) => (
                <div className="border-bottom mb-3 w-100" key={c.id}>
                  <div className="col-12 font-weight-bold">
                      {c.name}
                  </div>
                  <div className="col-12">
                      {c.body}
                  </div>
                </div>
            ))
        }
    </div>
  );
}
