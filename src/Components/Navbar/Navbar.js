import React from "react";
import { Link, useLocation } from "react-router-dom";

export default function Navbar() {
    let route = useLocation().pathname;
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to="/">
        Navbar
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className={route === "/" ? "nav-item active" : "nav-item"}>
            <Link className="nav-link" to="/">
              Memes
            </Link>
          </li>
          <li className={route === "/posts" ? "nav-item active" : "nav-item"}>
            <Link className="nav-link" to="/posts">
              Posts
            </Link>
          </li>
        </ul>
        <form className="form-inline my-2 my-lg-0">
          <input
            className="form-control mr-sm-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
          />
          <button
            className="btn btn-outline-success my-2 my-sm-0"
            type="button"
          >
            Buscar
          </button>
        </form>
      </div>
    </nav>
  );
}
